Parse.Cloud.define('hello', function(req, res) {
    return 'Hi';
});

Parse.Cloud.define('shpay', async(req) => {
    // Class Objects
    var User = Parse.Object.extend("User");
    var Transactions = Parse.Object.extend("Transaction");

    // User Query
    var BaseFee;
    var SDate = new Date();
    var EDate = new Date();

    var innerQuery = new Parse.Query(User);
    innerQuery.equalTo("objectId", req.params.id);
    const results = await innerQuery.find();

    // Do something with the returned Parse.Object values
    for (let i = 0; i < results.length; i++) {
        BaseFee = Number(results[i].get("Membership_Fee"));
        SDate = results[i].createdAt;
    }

    // Calculate Months
    var months;
    months = (EDate.getFullYear() - SDate.getFullYear()) * 12;
    months -= SDate.getMonth() + 1;
    months += EDate.getMonth();
    if (months <= 0) {
        months = 0;
    }

    // Transactions Query
    var query = new Parse.Query(Transactions);
    query.matchesQuery("User", innerQuery);
    query.equalTo("Reason", 0);
    const res = await query.find();
    var SumOfPay = 0;
    for (var i = 0; i < res.length; i++) {
        var sum = res[i].get("Value");
        SumOfPay += Number(sum);
    }

    //Final Calculate
    var total = (BaseFee * months) - SumOfPay;
    return total;
});

Parse.Cloud.define('Transactions', async(req) => {
    var User = Parse.Object.extend("User");
    var Transactions = Parse.Object.extend("Transaction");

    var innerQuery = new Parse.Query(User);
    innerQuery.equalTo("objectId", req.params.id);

    var query = new Parse.Query(Transactions);
    query.matchesQuery("User", innerQuery);
    var res = await query.find();

    return res;
});

Parse.Cloud.define('Campaigns', async(req) => {
    var result = [];
    var today = new Date();
    var edate = new Date();
    var Campaign = Parse.Object.extend("Campaigns");
    var query = new Parse.Query(Campaign);
    query.ascending("createdAt");
    var campaigns = await query.find();

    for (var i = 0; i < campaigns.length; i++) {
        var r = campaigns[i].get("End_Date");
        edate = new Date(Date.parse(r));
        if (edate >= today) {
            result.push(campaigns[i]);
        }
    }

    return result;
});

Parse.Cloud.define('Donate', async(req) => {

    var result;
    var Campaign = Parse.Object.extend("Campaigns");
    var User_Camp = Parse.Object.extend("User_Campaigns");
    var User = Parse.Object.extend("User");

    var Cquery = new Parse.Query(Campaign);
    Cquery.equalTo("objectId", req.params.cid);
    var camp = await Cquery.first();
    var x = camp.get("Progress");
    var f = (parseInt(x) + parseInt(req.params.amount));
    camp.set("Progress", f.toString());
    camp.save();

    var innerQuery = new Parse.Query(User);
    innerQuery.equalTo("objectId", req.params.uid);
    var sQuery = new Parse.Query(Campaign);
    sQuery.equalTo("objectId", req.params.cid);

    var query = new Parse.Query(User_Camp);
    query.matchesQuery("User", innerQuery);
    query.matchesQuery("Campaign", sQuery);
    result = query.find();
    return result;
});

Parse.Cloud.define('MyCampaigns', async(req) => {
    var User = Parse.Object.extend("User");
    var User_Camp = Parse.Object.extend("User_Campaigns");

    var innerQuery = new Parse.Query(User);
    innerQuery.equalTo("objectId", req.params.id);

    var query = new Parse.Query(User_Camp);
    query.matchesQuery("User", innerQuery);
    var res = await query.find();

    return res;
});

Parse.Cloud.define('getTickets', async(req) => {
    var Ticket = Parse.Object.extend("Ticket");
    var User = Parse.Object.extend("User");

    var innerQuery = new Parse.Query(User);
    innerQuery.equalTo("objectId", req.params.id);

    var query = new Parse.Query(Ticket);
    query.matchesQuery("User", innerQuery);
    query.descending("createdAt");
    var res = await query.find();

    return res;
});

Parse.Cloud.define('getTicket', async(req) => {
    var Ticket = Parse.Object.extend("Ticket");
    var User_Ticket = Parse.Object.extend("User_Ticket");

    var innerQuery = new Parse.Query(Ticket);
    innerQuery.equalTo("objectId", req.params.id);

    var query = new Parse.Query(User_Ticket);
    query.matchesQuery("Ticket", innerQuery);
    query.ascending("createdAt");
    var res = await query.find();

    return res;
});

Parse.Cloud.define('Pay', async(req) => {
    const MellatCheckout = require('mellat-checkout');
    const Transaction = Parse.Object.extend("Transaction");

    var result;
    var timestamp = new Date().getUTCMilliseconds();

    const trans = new Transaction();
    trans.set("Value", req.params.amount);
    trans.set("Reason", parseInt(req.params.res));
    trans.set("User", { "__type": "Pointer", "className": "_User", "objectId": req.params.id });
    trans.set("orderId", timestamp);

    await trans.save()
        .then((trans) => {
            const mellat = new MellatCheckout({
                terminalId: '2250128',
                username: 'bnd45',
                password: '78071050',
                timeout: 900000, // Optional, number in millisecond (defaults to 10 sec)
                apiUrl: 'https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl', // Optional, exists (and may updated) in bank documentation (defaults to this)
            });

            mellat.initialize().then(function() {
                    result = "Mellat client ready";
                    console.log("Mellat client ready");
                })
                .catch(error => {
                    // retry here
                    result = "Mellat client encountered error:" + error;
                    console.log("Mellat client encountered error:" + error);
                });

            mellat.paymentRequest({
                amount: req.params.amount,
                orderId: timestamp,
                callbackUrl: 'http://87.236.213.105:1337/callback'
            }).then(function(response) {
                if (response.resCode === 0) {
                    trans.set('RefId', response.refId);
                    trans.set('State', response.resCode);
                    trans.save();
                    result = response.refId;
                    console.log(response.refId);
                } else {
                    trans.set('State', response.resCode);
                    result = 'Gateway Error: ' + response.resCode;
                    console.log('Gateway Error: ' + response.resCode);
                }
            }).catch(function(error) {
                result = error;
                console.log(error);
            });
        });
    return result;
});